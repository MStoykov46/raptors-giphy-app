let url = 'http://api.giphy.com/v1/gifs';
let upload_url = 'http://upload.giphy.com/v1/gifs';
let apiKey = 'api_key=PCyK5EfZKsuvoBzFUSc5YL5FkYSAdpHw';

const getRandomGif = async () => {
    try {
        let response = await fetch(url + `/random?${apiKey}`);
    
        let body = await response.json();
        
        return body.data;
    } catch (error) {
        console.error(error);
    }
   
}

const uploadFile = async (fileFormData) => {

    try {
        let response = await fetch(upload_url + `?${apiKey}`, {
            method: 'POST',
            body: fileFormData
        });
    
        return await response.json();
    } catch (error) {
        console.error(error);
    }

    
}

const getById = async (id) => {
    try {
        let response = await fetch(url + `/${id}?${apiKey}`);
        return await response.json();
    } catch (error) {
        console.error(error);
    }
}

export { getRandomGif, uploadFile, getById };