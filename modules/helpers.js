const displayGif = ($container, imgData) => {
    let $imgContainer = $(`<div class="col-sm card" style="width: 18rem; id="${imgData.id}">
    <img src="${imgData.images.fixed_height_downsampled.url}" class="card-img-top" alt="${imgData.title}">
    <div class="card-body">
      <h5 class="card-title">${imgData.title}</h5>
      <h5 class="card-subtitle text-muted">Author: ${imgData.username}</h5>
      <i class="fas fa-heart fa-5x favourite-heart" id="${imgData.id}"></i>
    </div>
  </div>`);

    $container.append($imgContainer);

}

export { displayGif };