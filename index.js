// import { getTrending, addFavourite, uploadGif, myUploads } from  './modules/events.js';
import { attachEvent } from './modules/events.js';
import { getRandomGif, uploadFile, getById } from './modules/requests.js';
import { displayGif } from './modules/helpers.js'

function initGifs() {
    
}

(() => {
    // app logic goes here

    // This function will execute once
    initGifs();
    // attach events
    attachEvent('click', '#trendings-btn', async () => {
        let $container = $('#main-container');
        // $container.empty();
        let imgData = await getRandomGif();
        displayGif($container, imgData);
    });

    attachEvent('click', '.favourite-heart', async (event) => {
        console.log(event);
        console.log(event.target);
        let $el = $(event.target);
        
    });

    attachEvent('click', '#upload-file-btn', async () => {
        let files = $('#upload-file').prop('files');
        if(files.length == 0) {
            // notify user
            console.error('No files selected');
            return;
        }
        console.log(files[0]);
        let myFileFormData = new FormData();
        myFileFormData.append('file', files[0]);
        let responseBody = await uploadFile(myFileFormData);
        console.log(responseBody);
        if (responseBody.meta.msg === 'OK' && responseBody.meta.status === 200) {
            let imageId = responseBody.data.id;
            let myUploadsIds = localStorage.getItem('myUploads');
            localStorage.setItem('myUploads', myUploadsIds === null ? imageId : myUploadsIds + `,${imageId}` );
        }
    });

    attachEvent('click', '#my-uploads', () => {
        let ids = localStorage.getItem('myUploads');

        if(ids != null) {
            ids.split(',').map(async (id) => {
                let imageData = await getById(id);
                console.log(imageData);
                displayGif($('#main-container'), imageData.data);
                return imageData;
            });
        }
    });
})()